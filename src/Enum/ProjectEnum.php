<?php

namespace App\Enum;

enum ProjectEnum: string
{
    case Default = 'default';

    case Liberec = 'liberec';

    case Brno = 'brno';

    public static function createFromValue(string $projectName): ProjectEnum
    {
        return self::tryFrom(strtolower($projectName)) ?? self::Default;
    }
}
