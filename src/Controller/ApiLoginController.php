<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class ApiLoginController extends AbstractController
{
    #[Route('/api/login', name: 'api_login', methods: ['POST'], format: 'json')]
    public function login(#[CurrentUser] User $user): JsonResponse
    {
        return $this->json([
            'message' => 'Success',
            'user' => $user->getEmail(),
        ]);
    }

    #[Route('/api/profile', name: 'api_profile', methods: ['GET'], format: 'json')]
    public function profile(#[CurrentUser] User $user): JsonResponse
    {
        return $this->json([
            'message' => 'Success',
            'user' => $user->getEmail(),
        ]);
    }

    #[Route('/api/logout', name: 'api_logout', methods: ['POST'], format: 'json')]
    public function logout(): void
    {
    }
}
