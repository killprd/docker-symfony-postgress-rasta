<?php

namespace App\Dto;

class ProjectInfoDto
{
    const PROJECT_DEFAULT = 'default';

    const PROJECT_LIBEREC = 'liberec';

    const PROJECT_BRNO = 'brno';


    protected string $project = self::PROJECT_DEFAULT;

    /**
     * @return string|null
     */
    public function getProject(): string
    {
        return $this->project;
    }

    /**
     * @param string|null $project
     */
    public function setProject(?string $project = self::PROJECT_DEFAULT): void
    {
        $this->project = self::PROJECT_DEFAULT;

        if (in_array($project, $this->getProjects(), true)) {
            $this->project = $project;
        }
    }

    public function getProjects(): array
    {
        return [
            self::PROJECT_DEFAULT,
            self::PROJECT_LIBEREC,
            self::PROJECT_BRNO,
        ];
    }
}
