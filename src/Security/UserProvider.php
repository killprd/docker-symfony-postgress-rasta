<?php

namespace App\Security;

use App\Doctrine\EntityRepositoryProvider;
use App\Doctrine\ProjectProvider;
use App\Entity\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface, PasswordUpgraderInterface
{
    public function __construct(
        protected EntityRepositoryProvider $entityRepositoryProvider,
        protected ProjectProvider $projectProvider,
    ) {
    }

    public function loadUserByIdentifier(?string $identifier = null): UserInterface
    {
        $projectEnum = $this->projectProvider->getProjectEnum();
        $repository = $this->entityRepositoryProvider->getEntityRepository(User::class, $projectEnum->value);

        $user = $repository->findOneBy(['email' => $identifier]);

        if (!$user) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * Refreshes the user after being reloaded from the session.
     *
     * When a user is logged in, at the beginning of each request, the
     * User object is loaded from the session and then this method is
     * called. Your job is to make sure the user's data is still fresh by,
     * for example, re-querying for fresh User data.
     *
     * If your firewall is "stateless: true" (for a pure API), this
     * method is not called.
     *
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        $projectEnum = $this->projectProvider->getProjectEnum();
        $repository = $this->entityRepositoryProvider->getEntityRepository(User::class, $projectEnum->value);

        return $repository->findOneBy(['email' => $user->getEmail()]);
    }

    /**
     * Tells Symfony to use this provider for this User class.
     */
    public function supportsClass(string $class): bool
    {
        return User::class === $class || is_subclass_of($class, User::class);
    }

    /**
     * Upgrades the hashed password of a user, typically for using a better hash algorithm.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        $projectEnum = $this->projectProvider->getProjectEnum();
        $documentManager = $this->entityRepositoryProvider->getDocumentManager($projectEnum->value);

        $user->setPassword($newHashedPassword);
        $documentManager->flush();
    }
}
