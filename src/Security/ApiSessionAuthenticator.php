<?php

namespace App\Security;

use App\Doctrine\ProjectProvider;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class ApiSessionAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        protected ProjectProvider $projectProvider,
        protected UserProviderInterface $userProvider,
    ) {
    }

    public function supports(Request $request): ?bool
    {
        $route = $request->attributes->get('_route');
        return $route !== 'api_login';
    }

    public function authenticate(Request $request): Passport
    {
        $this->projectProvider->resolveProject($request);

        /** @var ?User $user */
        $user = $request->getSession()->get('userIdentity');

        return new SelfValidatingPassport(new UserBadge((string) $user?->getEmail(), function($identifier) {
            if ($identifier && ($user = $this->userProvider->loadUserByIdentifier($identifier))) {
                return $user;
            }

            throw new UserNotFoundException();
        }));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return null;
    }
}
