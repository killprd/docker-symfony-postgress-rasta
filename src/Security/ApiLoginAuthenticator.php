<?php

namespace App\Security;

use App\Doctrine\ProjectProvider;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class ApiLoginAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        protected ProjectProvider             $projectProvider,
        protected SerializerInterface $serializer,
        protected UserProviderInterface $userProvider,
    ) {
    }

    public function supports(Request $request): ?bool
    {
        $route = $request->attributes->get('_route');
        return $route === 'api_login';
    }

    public function authenticate(Request $request): Passport
    {
        $this->projectProvider->resolveProject($request);

        /** @var User $userData */
        $userData = $this->serializer->deserialize($request->getContent() ?: '{}', User::class, 'json', [
            AbstractNormalizer::ATTRIBUTES => ['email', 'password'],
        ]);

        $email = (string) $userData->getEmail();
        $plainPass = (string) $userData->getPassword();

        if (!($email && $plainPass)) {
            throw new CustomUserMessageAuthenticationException('Missing credentials');
        }

        return new Passport(
            new UserBadge($email, function ($userIdentifier) {
                return $this->userProvider->loadUserByIdentifier($userIdentifier);
            }),
            new PasswordCredentials($plainPass)
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        if ($user = $token->getUser()) {
            $request->getSession()->set('userIdentity', $user);
        }

        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $message = \strtr($exception->getMessageKey(), $exception->getMessageData());


        return new JsonResponse([
            'message' => $message,
        ], Response::HTTP_UNAUTHORIZED);
    }
}
