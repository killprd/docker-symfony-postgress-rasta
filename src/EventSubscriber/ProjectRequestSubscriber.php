<?php

namespace App\EventSubscriber;

use App\Doctrine\ProjectProvider;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class ProjectRequestSubscriber implements EventSubscriberInterface
{
    public function __construct(
        protected ProjectProvider $projectProvider,
    ) {
    }

    public static function getSubscribedEvents()
    {
        return [
            RequestEvent::class => ['onKernelRequest', 0],
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $this->projectProvider->resolveProject($request);
    }
}
