<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Event\LoginFailureEvent;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        protected LoggerInterface $logger,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ExceptionEvent::class => ['onException', 8],
        ];
    }

    public function onException(ExceptionEvent $event): void
    {
        $e = $event->getThrowable();

        if ($e instanceof \Error) {
            $this->logger->error($e->getMessage());
            $message = 'An error occured.' . $e->getMessage();
        } else {
            $message = $e->getMessage();
        }

        if ($e instanceof HttpExceptionInterface) {
            $statusCode = $e->getStatusCode();
        } else {
            $code = $e->getCode();
            $statusCode = in_array($code, array_keys(Response::$statusTexts), true) ? $code : Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        $event->setResponse(new JsonResponse([
            'message' => $message,
        ], $statusCode));

        $event->stopPropagation();
    }
}
