<?php

namespace App\Doctrine;

use App\Dto\ProjectInfoDto;
use App\Enum\ProjectEnum;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\SerializerInterface;

class ProjectProvider
{
    private ?ProjectEnum $projectEnum = ProjectEnum::Default;

    public function __construct(
        protected RequestStack $requestStack,
        protected SerializerInterface $serializer,
    ) {
    }

    public function getProjectEnum(): ProjectEnum
    {
        return $this->projectEnum;
    }

    public function resolveProject(?Request $request = null): void
    {
        $request = $request ?? $this->requestStack->getMainRequest();

        if (!$request) {
            return;
        }

        $routeName = $request->attributes->get('_route');
        $postRoutesWithChangeProject = ['api_login'];

        if (in_array($routeName, $postRoutesWithChangeProject, true)) {
            /** @var ProjectInfoDto $projectInfoDto */
            $projectInfoDto = $this->serializer->deserialize($request->getContent() ?: '{}', ProjectInfoDto::class, 'json');
            $this->projectEnum = ProjectEnum::createFromValue((string) $projectInfoDto->getProject());
        } else {
            $this->projectEnum = ProjectEnum::createFromValue($request->getSession()->get('project', ''));
        }

        $request->getSession()->set('project', $this->projectEnum->value);
    }
}
