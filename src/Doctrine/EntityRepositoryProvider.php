<?php

namespace App\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class EntityRepositoryProvider
{
    public function __construct(
        protected ManagerRegistry $managerRegistry,
    ) {
    }

    public function getDocumentManager(?string $entityManagerName = null): EntityManagerInterface
    {
        return match ($entityManagerName) {
            'liberec' => $this->managerRegistry->getManager('liberec'),
            'brno' => $this->managerRegistry->getManager('brno'),
            default => $this->managerRegistry->getManager(),
        };
    }

    public function getEntityRepository(string $entityClassName, ?string $entityManagerName = null): EntityRepository
    {
        $entityManager =$this->getDocumentManager($entityManagerName);

        if (!class_exists($entityClassName)) {
            throw new \RuntimeException('Unknown doctrine entity');
        }

        return $entityManager->getRepository($entityClassName);
    }
}
