<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'user:create',
    description: 'Create new user',
)]
class CreateUserCommand extends Command
{
    public function __construct(
        private UserPasswordHasherInterface $passwordHasher,
        private ManagerRegistry $managerRegistry,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(
            'entityManagerName',
            InputArgument::OPTIONAL,
            'Name of entity manager (default, liberec, brno).'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {

            $entityManagerName = $input->getArgument('entityManagerName');

            $email = $io->ask('Email', validator: function ($outputEmail) {
                if (empty($outputEmail)) {
                    throw new \RuntimeException('Email is required.');
                }

                return $outputEmail;
            });

            $plainPass = $io->askHidden('Password', function ($outputPass) {
                if (empty($outputPass)) {
                    throw new \RuntimeException('Password is required.');
                }

                return $outputPass;
            });

            $user = new User();
            $user->setEmail($email);
            $user->setPassword($this->passwordHasher->hashPassword($user, $plainPass));

            $this->getUserRepository($entityManagerName)->save($user, true);

            $io->writeln(sprintf('User %s was successfully created', $email));

            return Command::SUCCESS;

        } catch (\Throwable $e) {

            $io->writeln($e->getMessage());

            return Command::FAILURE;
        }
    }

    protected function getUserRepository(string $entityManagerName)
    {
        $entityManager = match ($entityManagerName) {
            'liberec' => $this->managerRegistry->getManager('liberec'),
            'brno' => $this->managerRegistry->getManager('brno'),
            default => $this->managerRegistry->getManager(),
        };

        return $entityManager->getRepository(User::class);
    }
}
