up:
	docker-compose up -d

down:
	docker-compose down

sh:
	docker-compose exec -it php //bin//sh

composer:
	docker-compose exec -it php composer $(filter-out $@,$(MAKECMDGOALS))
